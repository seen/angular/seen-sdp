# Seen SDP

A library to communicate with Seen servers which support the SDP (Sell Digital Products) module.

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-sdp/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-sdp/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9c63efb4959b44b8bd6a846517047c5e)](https://www.codacy.com/app/seen/seen-sdp?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-sdp&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-sdp/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-sdp/commits/develop)

## Install

To install by bower run the following command:

	bower install seen-sdp --save

There is two folder which are contained main files of project:

- src
- dist

## Develop


### Install Tools

The needed tools to develop this project are managed by nodejs. To install these tools run the following command:

	npm install

After that, the dependencies should be installed. The following command do this for you:

	bower install

### Generate Product

To generate product of the project run the following command:

	grunt build

## Test & Debug

To run the tests of the project following command could be executed:

	grunt test

and for debugging the project the following command could be run:

	grunt debug
