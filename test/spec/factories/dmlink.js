///* jslint todo: true */
///* jslint xxx: true */
///* jshint -W100 */
//'use strict';
//
///*
// * تعریف یک دسته از تست‌ها اینجوری هست: پارامتر اول عنوان تست‌ها رو تعیین می کنه
// * و پارامتر دوم یک تابع هست که همه تست‌ها توی اون تعریف می‌شه.
// */
//describe('Controller: DmLink', function() {
//
//	/*
//	 * بدنه تابع باید شامل تنظمی‌ها و تست‌هایی باشه که برای کار لازم داریم.
//	 * 
//	 * تست ماژول‌های انگولار نیاز به یه سری کارها داره که باید قبل از نوشت
//	 * برنامه تست انجام بدیم.
//	 */
//
//	/*
//	 * قبل از شروع باید متغیرهایی تعریف کنیم و توی این متغیرها قسمت‌هایی که
//	 * می‌خواهیم تست کنیم رو نگهداریم. من اسم این متغیرها رو همنام با بخشی‌هایی
//	 * قرار دادم که می‌خوام تست کنم.
//	 */
//	var DmLink;
//	var $rootScope;
//	var $httpBackend;
//
//	/*
//	 * اولین کار لود کردن ماژولی است که می‌خواهیم تست کنیم. کار لودکردن ماژول
//	 * باید قبل از اجرای هر تست باشه پس باید به صورت زیر انجام بشه.
//	 */
//	beforeEach(module('seen-sdp'));
//
//	/*
//	 * کار دیگه‌ای که قبل از هر تست باید انجام بشه، گرفتن بخشی از ماژول هست که
//	 * می‌خواهیم تست کنیم. این کار برای تست کنترلر و یا فکتوری فرق داره مثلا
//	 * برای تست کنترلر باید سرویس کنترلر رو گرفت از اون کنترل مورد نیاز رو لود
//	 * کرد.
//	 */
//	beforeEach(module('seen-sdp'));
//	beforeEach(inject(function(_DmLink_, _$rootScope_, _$httpBackend_) {
//		DmLink = _DmLink_;
//		$rootScope = _$rootScope_;
//		$httpBackend = _$httpBackend_;
//	}));
//
//	it('Should ', function() {
//		var dmlink = new DmLink();
//		expect(angular.isFunction(dmlink.update)).toBe(true);
//	});
//
//	it('Should ', function() {
//		var dmlink = new DmLink();
//		expect(angular.isFunction(dmlink.delete)).toBe(true);
//	});
//	it('Sould call a rest', function(done) {
//		var dmlink = new DmLink({id: 1});
//		dmlink.delete()//
//		.then(function() {
//			done();
//		});
//		/*
//		 * 
//		 *  *
//		 */
//		$httpBackend//
//		.expect('DELETE', '/api/dm/link/' + dmlink.id)//
//		.respond(200, {
//			id : 1
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//});
