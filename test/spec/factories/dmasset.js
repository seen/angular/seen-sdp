///* jslint todo: true */
///* jslint xxx: true */
///* jshint -W100 */
//'use strict';
//
//describe('Controller:DmAsset', function() {
//	var DmAsset;
//	var $rootScope;
//	var $httpBackend;
//
//	beforeEach(module('seen-sdp'));
//	beforeEach(inject(function(_DmAsset_, _$rootScope_, _$httpBackend_) {
//		DmAsset = _DmAsset_;
//		$rootScope = _$rootScope_;
//		$httpBackend = _$httpBackend_;
//	}));
//
//	it('Check function', function() {
//		var dmAsset = new DmAsset();
//		expect(angular.isFunction(dmAsset.update)).toBe(true);
//	});
//	it('should call /dm/asset/{id} REST', function(done) {
//		var id = 1;
//		var dmAsset = new DmAsset({
//			id : 1
//		});
//		dmAsset.update()//
//		.then(function(dmAsset) {
//			expect(dmAsset).not.toBeNull();
//			done();
//		});
//		$httpBackend//
//		.expect('POST', '/api/dm/asset/' + id)//
//		.respond(200);
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//
//	it('Check function', function() {
//		var dmAsset = new DmAsset({
//			id : 1
//		});
//		expect(angular.isFunction(dmAsset.delete)).toBe(true);
//	});
//	it('should call /dm/asset/{id} REST', function(done) {
//		var id = 1;
//		var dmAsset = new DmAsset({
//			id : 1
//		});
//		dmAsset.delete()//
//		.then(function(dmAsset) {
//			expect(dmAsset).not.toBeNull();
//			done();
//		});
//		$httpBackend//
//		.expect('DELETE', '/api/dm/asset/' + id)//
//		.respond(200);
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//});