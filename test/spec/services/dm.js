///* jslint todo: true */
///* jslint xxx: true */
///* jshint -W100 */
//'use strict';
//
///*
// * دستور
// * describe
// * برای تعریف یک تابع تست مورد استفاده قرار می‌گیرد که دارای دو پارامتر ورودی است
// * پارامتر اول عنوان مجموعه تست‌هاست پارامتر دوم تابعی است که به آن 
// * Spec
// * می گویند و در آن
// * assert
// * هایی تعریف می‌شود که با 
// * it
// * مشخص می‌شوند
// * و آ‌ن‌ها نیز درای دو پارامتر هستند اولی عنوان تست و دومی تابعی که شامل 
// * expect
// * است که بنابر نوع تست از انواع مختلف آن استفاده می‌کنیم
// * 
// */
//describe('In dm service', function() {
//	var service;
//	var $httpBackend;
//	var $rootScope;
//
//	beforeEach(module('seen-sdp'));
//	beforeEach(inject(function($dm, _$rootScope_, _$httpBackend_) {
//		service = $dm;
//		$rootScope = _$rootScope_;
//		$httpBackend = _$httpBackend_;
//	}));
//
//	/*
//	 * تست وجود متد
//	 * link
//	 * در سرویس
//	 * dm
//	 */
//	it('should define link method', function() {
//		expect(service.link).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/link/{id}
//	 */
//	it('should link method call /dm/link/{id} REST', function(done) {
//		var id = 'testid';
//		service.link(id).then(function(link) {
//			expect(link).not.toBeNull();
//			expect(link.id).toBe(id);
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/link/' + id)//
//		.respond(200, {
//			'id' : id,
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//	
//	/*
//	 * تست وجود متد
//	 * links
//	 * در سرویس
//	 * dm
//	 */
//	it('links method undefine', function() {
//		expect(service.links).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/link/find
//	 */
//	it('should links method call /dm/link/find REST', function(done) {
//		service.links().then(function(link) {
//			expect(link).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/link/find')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//
//	/*
//	 * تست وجود متد
//	 * createLink
//	 * در سرویس
//	 * dm
//	 */
//	it('createlink method undefined', function() {
//		expect(service.createLink).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/link
//	 */
//	it('should createLink method call /dm/link REST', function(done) {
//		service.createLink().then(function(link) {
//			expect(link).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('POST', '/api/dm/link')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//
//	/*
//	 * تست وجود متد
//	 * asset
//	 * در سرویس
//	 * dm
//	 */
//	it('asset method undefine', function() {
//		expect(service.asset).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/{id}
//	 */
//	it('should asset method call /dm/asset/{id} REST', function(done) {
//		var id = 'aa';
//		service.asset(id)//
//		.then(function(asset) {
//			expect(asset).not.toBeNull();
//			expect(asset.id).toBe(id);
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/asset/' + id)//
//		.respond(200, {
//			'id' : id,
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});	
//	
//	/*
//	 * تست وجود متد
//	 * assets
//	 * در سرویس
//	 * dm
//	 */
//	it('assets method undefine', function() {
//		expect(service.assets).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/find
//	 */
//	it('should assets method call /dm/asset/find REST', function(done) {
//		service.assets().then(function(asset) {
//			expect(asset).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/asset/find')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//
//	/*
//	 * تست وجود متد
//	 * createAsset
//	 * در سرویس
//	 * dm
//	 */
//	it('createAsset method undefine', function() {
//		expect(service.createAsset).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/new
//	 */
//	it('should createAsset method call /dm/asset/new REST', function(done) {
//		service.createAsset().then(function(asset) {
//			expect(asset).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('POST', '/api/dm/asset/new')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//	
//	/*
//	 * تست وجود متد
//	 * plan
//	 * در سرویس
//	 * dm
//	 */
//	it('plan method undefine', function() {
//		expect(service.plan).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/plan/{id}
//	 */
//	it('should plan method call /dm/plan/{id} REST', function(done) {
//		var id = 'gg';
//		service.plan(id)//
//		.then(function(plan) {
//			expect(plan).not.toBeNull();
//			expect(plan.id).toBe(id);
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/plan/' + id)//
//		.respond(200, {
//			'id' : id,
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});	
//
//	/*
//	 * تست وجود متد
//	 * plans
//	 * در سرویس
//	 * dm
//	 */
//	it('plans method undefine', function() {
//		expect(service.plans).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/plan/find
//	 */
//	it('should plans method call /dm/plan/find REST', function(done) {
//		service.plans().then(function(plan) {
//			expect(plan).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/plan/find')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//
//	/*
//	 * تست وجود متد
//	 * createPlan
//	 * در سرویس
//	 * dm
//	 */
//	it('createPlan method undefine', function() {
//		expect(service.createPlan).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/plan/new
//	 */
//	it('should createPlan method call /dm/plan/new REST', function(done) {
//		service.createPlan().then(function(plan) {
//			expect(plan).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('POST', '/api/dm/plan/new')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//
//	/*
//	 * تست وجود متد
//	 * planTemplate
//	 * در سرویس
//	 * dm
//	 */
//	it('planTemplate method undefine', function() {
//		expect(service.planTemplate).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/planTemplate/{id}
//	 */
//	it('should planTemplate method call /dm/planTemplate/{id} REST', function(done) {
//		var id = 'hh';
//		service.planTemplate(id)//
//		.then(function(planTemplate) {
//			expect(planTemplate).not.toBeNull();
//			expect(planTemplate.id).toBe(id);
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/planTemplate/' + id)//
//		.respond(200, {
//			'id' : id,
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});	
//
//	/*
//	 * تست وجود متد
//	 * planTemplates
//	 * در سرویس
//	 * dm
//	 */
//	it('planTemplates method undefine', function() {
//		expect(service.planTemplates).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/planTemplate/find
//	 */
//	it('should planTemplates method call /dm/planTemplate/find REST', function(done) {
//		service.planTemplates().then(function(planTemplate) {
//			expect(planTemplate).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('GET', '/api/dm/planTemplate/find')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//
//	/*
//	 * تست وجود متد
//	 * createPlanTemplate
//	 * در سرویس
//	 * dm
//	 */
//	it('createPlanTemplate method undefine', function() {
//		expect(service.createPlanTemplate).toBeDefined();
//	});
//	/*
//	 * تست بازگشت
//	 * response.
//	 * از سرور با ازسال
//	 * rest
//	 * با مقدار
//	 * /dm/planTemplate/new
//	 */
//	it('should createPlanTemplate method call /dm/planTemplate/new REST', function(done) {
//		service.createPlanTemplate().then(function(planTemplate) {
//			expect(planTemplate).not.toBeNull();
//			done();
//		});
//		
//		$httpBackend//
//		.expect('POST', '/api/dm/planTemplate/new')//
//		.respond(200, {
//			'title' : 'example title'
//		});
//		expect($httpBackend.flush).not.toThrow();
//		$rootScope.$apply();
//	});
//});
