/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * @ngdoc overview
 * @name seen-sdp
 * @description Seen SDP module
 */
angular.module('seen-sdp', [ 'seen-core', 'seen-bank' ])

/**
 * @ngdoc Factories
 * @name SdpAsset
 * @description SDP Asset
 * 
 * @attr {integer} id of the asset
 */
.factory('SdpAsset', seen.factory({
    url : '/api/v2/sdp/assets',
    resources : [{
        name : 'Content',
        type : 'binary',
        url : '/content'
    },
    {
        name : 'Tag',
        factory : 'SdpTag',
        type : 'collection',
        url : '/tags'
    }, {
        name : 'Category',
        factory : 'SdpCategory',
        type : 'collection',
        url : '/categories'
    }, {
        name : 'Link',
        factory : 'SdpLink',
        type : 'collection',
        url : '/links'
    }, {
        name : 'AssetRelation',
        factory : 'SdpAssetRelation',
        type : 'collection',
        url : '/relations'
    }, {
        name : 'Child',
        factory : 'SdpAsset',
        type : 'collection',
        url : '/children'
    } ]
}))

/**
 * @ngdoc Factories
 * @name SdpAssetRelation
 * @description Defines relation between assets
 */
.factory('SdpAssetRelation', seen.factory({
    url : '/api/v2/sdp/asset-relations'
}))

/**
 * @ngdoc Factories
 * @name SdpCategory
 * @description Defines a category
 */
.factory('SdpCategory', seen.factory({
    url : '/api/v2/sdp/categories',
    resources : [ {
        name : 'Asset',
        factory : 'SdpAsset',
        type : 'collection',
        url : '/assets'
    } ]
}))

/**
 * @ngdoc Factories
 * @name SdpTag
 * @description Defines a tag
 */
.factory('SdpTag', seen.factory({
    url : '/api/v2/sdp/tags',
    resources : [ {
        name : 'Asset',
        factory : 'SdpAsset',
        type : 'collection',
        url : '/assets',
    } ]
}))

/**
 * @ngdoc Factories
 * @name SdpDrive
 * @description Defines a drive which stores assets` files
 */
.factory('SdpDrive', seen.factory({
    url : '/api/v2/sdp/drives'
}))

/**
 * @ngdoc Factories
 * @name SdpDriver
 * @description Defines a driver to work with drives which store assets` files
 */
.factory('SdpDriver', seen.factory({
    url : '/api/v2/sdp/drivers'
}))

/**
 * @ngdoc Factories
 * @name SdpLink
 * @description Defines a link to download the file of an asset
 */
.factory('SdpLink', seen.factory({
    url : '/api/v2/sdp/links',
    resources : [ {
        name : 'Content',
        type : 'binary',
        url : '/content'
    },{
        name: 'Payment',
        factory: 'BankReceipt',
        type: 'collection',
        url: '/payments'
    } ]
}))

/**
 * @ngdoc Factories
 * @name SdpProfile
 * @description Defines sdp profile
 */
.factory('SdpProfile', seen.factory({
    url : '/api/v2/sdp/accounts/{account_id}/profiles'
}))

/**
 * @ngdoc Services
 * @name $sdp
 * @description Manages entities in the SDP
 */
.service('$sdp', seen.service({
    resources : [ {
        name : 'Asset',
        factory : 'SdpAsset',
        type : 'collection',
        url : '/api/v2/sdp/assets'
    }, {
        name : 'Category',
        factory : 'SdpCategory',
        type : 'collection',
        url : '/api/v2/sdp/categories'
    }, {
        name : 'Tag',
        factory : 'SdpTag',
        type : 'collection',
        url : '/api/v2/sdp/tags'
    }, {
        name: 'Link',
        factory: 'SdpLink',
        type: 'collection',
        url: '/api/v2/sdp/links'
    }, {
        name : 'AssetRelation',
        factory : 'SdpAssetRelation',
        type : 'collection',
        url : '/api/v2/sdp/asset-relations'
    }, {
        name : 'Drive',
        factory : 'SdpDrive',
        type : 'collection',
        url : '/api/v2/sdp/drives'
    }, {
        name : 'Driver',
        factory : 'SdpDriver',
        type : 'collection',
        url : '/api/v2/sdp/drivers'
    } ]
}));
