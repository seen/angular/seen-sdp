///*
// * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//angular.module('seen-sdp')
///**
// * 
// */
//.service('$dm', function(seen, PObjectCache, DmLink, DmAsset, DmPlan, DmPlanTemplate) {
//
//	var _linkCache = new PObjectCache(function(data) {
//		return new DmLink(data);
//	});
//	var _assetCache = new PObjectCache(function(data) {
//		return new DmAsset(data);
//	});
//	var _planCache = new PObjectCache(function(data) {
//		return new DmPlan(data);
//	});
//	var _planTemplateCache = new PObjectCache(function(data) {
//		return new DmPlanTemplate(data);
//	});
//
//	/**
//	 * لینک معادل با شناسه ورودی را تعیین می‌کند
//	 * 
//	 * توی این پیاده سازی لینک ورودی به سرور ارسال می‌شه و نتیجه به دست آمده به
//	 * صورت یک موجودیت لینک ایجاد می‌شه.
//	 * 
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.link('linkId').then(function(link) {
//	 * 	// Do something with link
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @note نوشتن مستند فراموش نشود.
//	 * 
//	 * @return promise(DmLink)
//	 */
//	this.link = seen.createGet({
//		method : 'GET',
//		url : '/api/dm/link/{id}'
//	}, _linkCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.links('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(DmLink)
//	 */
//	this.links = seen.createFind({
//		method : 'GET',
//		url : '/api/dm/link/find',
//	}, _linkCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.createLink('linkData').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(DmLink)
//	 */
//	this.createLink = seen.createNew({
//		method : 'POST',
//		url : '/api/dm/link',
//	}, _linkCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.asset('assetId').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise()
//	 */
//	this.asset = seen.createGet({
//		method : 'GET',
//		url : '/api/dm/asset/{id}'
//	}, _assetCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.assets('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(DmAsset)
//	 */
//	this.assets = seen.createFind({
//		method : 'GET',
//		url : '/api/dm/asset/find',
//	}, _assetCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.createAsset('assetData').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(DmAsset)
//	 */
//	this.createAsset = seen.createNew({
//		method : 'POST',
//		url : '/api/dm/asset/new',
//	}, _assetCache);
//
//	this.plan = seen.createGet({
//		method : 'GET',
//		url : '/api/dm/plan/{id}'
//	}, _planCache);
//
//	this.plans = seen.createFind({
//		method : 'GET',
//		url : '/api/dm/plan/find',
//	}, _planCache);
//
//	this.createPlan = seen.createNew({
//		method : 'POST',
//		url : '/api/dm/plan/new',
//	}, _planCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.planTemplate('planTemplateId').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(DmPlanTemplate)
//	 */
//	this.planTemplate = seen.createGet({
//		method : 'GET',
//		url : '/api/dm/planTemplate/{id}'
//	}, _planTemplateCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.planTemplates('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(DmPlanTemplate)
//	 */
//	this.planTemplates = seen.createFind({
//		method : 'GET',
//		url : '/api/dm/planTemplate/find',
//	}, _planTemplateCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $dm.createPlanTemplate('planTemplatesData')
//	 * .then(function(result) { // Do something with result }); 
//	 * </code></pre>
//	 * 
//	 * @return promise(DmPlanTemplate)
//	 */
//	this.createPlanTemplate = seen.createNew({
//		method : 'POST',
//		url : '/api/dm/planTemplate/new',
//	}, _planTemplateCache);
//});
