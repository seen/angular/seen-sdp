///*
// * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//angular.module('seen-sdp')
///**
// * 
// */
//.service('$sdp', function(seen, PObjectCache, SdpLink, SdpAsset, SdpAssetRelation, SdpTag, SdpCategory,
//		$httpParamSerializerJQLike, $http) {
//
//	var _linkCache = new PObjectCache(function(data) {
//		return new SdpLink(data);
//	});
//	var _assetCache = new PObjectCache(function(data) {
//		return new SdpAsset(data);
//	});
//	var _assetRelationCache = new PObjectCache(function(data) {
//		return new SdpAssetRelation(data);
//	});
//	var _categoryCache = new PObjectCache(function(data) {
//		return new SdpCategory(data);
//	});
//	var _tagCache = new PObjectCache(function(data) {
//		return new SdpTag(data);
//	});
//
//	/**
//	 * لینک معادل با شناسه ورودی را تعیین می‌کند
//	 * 
//	 * توی این پیاده سازی لینک ورودی به سرور ارسال می‌شه و نتیجه به دست
//	 * آمده به صورت یک موجودیت لینک ایجاد می‌شه.
//	 * 
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.link('linkId').then(function(link) {
//	 * 	// Do something with link
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @note نوشتن مستند فراموش نشود.
//	 * 
//	 * @return promise(SdpLink)
//	 */
//	this.link = seen.createGet({
//		method : 'GET',
//		url : '/api/sdp/link/{id}'
//	}, _linkCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.links('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpLink)
//	 */
//	this.links = seen.createFind({
//		method : 'GET',
//		url : '/api/sdp/link/find',
//	}, _linkCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.createLink('assetId').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpLink)
//	 */
//	this.createLink = seen.createGet({
//		method : 'GET',
//		url : '/api/sdp/asset/{id}/link',
//	}, _linkCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.asset('assetId').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise()
//	 */
//	this.asset = seen.createGet({
//		method : 'GET',
//		url : '/api/sdp/asset/{id}'
//	}, _assetCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.assets('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpAsset)
//	 */
//	this.assets = seen.createFind({
//		method : 'GET',
//		url : '/api/sdp/asset/find',
//	}, _assetCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.createAsset('assetData').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpAsset)
//	 */
//	this.createAsset = seen.createNew({
//		method : 'POST',
//		url : '/api/sdp/asset/new',
//	}, _assetCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.createFileAsset('assetData', 'assetFile').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpAsset)
//	 */
//	this.createFileAsset = function(objectData, file) {
//		var formData = new FormData();
//		for(var key in objectData){
//			if(objectData[key]){
//				formData.append(key, objectData[key]);
//			}
//		}
//		formData.append('type', 'file');
//		formData.append('file', file);
//		return $http.post('/api/sdp/asset/new', formData, {
//			transformRequest : angular.identity,
//			headers : {
//				'Content-Type' : undefined
//			}
//		})//
//		.then(function(res) {
//			return _assetCache.restor(res.data.id, res.data);
//		});
//	};
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.category('categoryId').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpCategory)
//	 */
//	this.category = seen.createGet({
//		method : 'GET',
//		url : '/api/sdp/category/{id}'
//	}, _categoryCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.categories('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(PaginatorPage)
//	 */
//	this.categories = seen.createFind({
//		method : 'GET',
//		url : '/api/sdp/category/find',
//	}, _categoryCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.createCategory('categoryData').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpCategory)
//	 */
//	this.createCategory = seen.createNew({
//		method : 'POST',
//		url : '/api/sdp/category/new',
//	}, _categoryCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت‌های زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.tag('tagId').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * <pre><code>
//	 * $sdp.tag('tagName').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpTag)
//	 */
//	this.tag = seen.createGet({
//		method : 'GET',
//		url : '/api/sdp/tag/{id}'
//	}, _tagCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.tags('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(PaginatorPage)
//	 */
//	this.tags = seen.createFind({
//		method : 'GET',
//		url : '/api/sdp/tag/find',
//	}, _tagCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.createTag('categoryData').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpTag)
//	 */
//	this.createTag = seen.createNew({
//		method : 'POST',
//		url : '/api/sdp/tag/new',
//	}, _tagCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.assetRelation('assetRelationId').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise()
//	 */
//	this.assetRelation = seen.createGet({
//		method : 'GET',
//		url : '/api/sdp/assetrelation/{id}'
//	}, _assetRelationCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.assetRelations('paginatorParam').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpAsset)
//	 */
//	this.assetRelations = seen.createFind({
//		method : 'GET',
//		url : '/api/sdp/assetrelation/find',
//	}, _assetRelationCache);
//
//	/**
//	 * استفاده از این فراخوانی به صورت زیر است:
//	 * 
//	 * <pre><code>
//	 * $sdp.createAsset('assetData').then(function(result) {
//	 * 	// Do something with result
//	 * 	});
//	 * </code></pre>
//	 * 
//	 * @return promise(SdpAsset)
//	 */
//	this.createAssetRelation = seen.createNew({
//		method : 'POST',
//		url : '/api/sdp/assetrelation/new',
//	}, _assetRelationCache);
//	
//	/**
//	 * دسته‌هایی که یک دارایی در آن‌ها قرار دارد را برمی‌گرداند.
//	 * فراخوانی این تابع به صورت زیر است:
//	 * <pre><code>
//	 * 		$sdp.categoriesOfAsset('asset', 'pagitatorParam').then(function(result){
//	 * 			// Result is a PaginatorPage<SdpCategory>
//	 * 			// Do something with result.
//	 * 		});
//	 * </code></pre>
//	 */
//	this.categoriesOfAsset = function(asset, pp){
//		return seen.createFind({
//			method : 'GET',
//			url : '/api/sdp/asset/'+asset.id+'/category/find',
//		}, _categoryCache)(pp);
//	};
//	
//	/**
//	 * برچسب‌های زده شده روی یک دارایی را برمی‌گرداند.
//	 * فراخوانی این تابع به صورت زیر است:
//	 * <pre><code>
//	 * 		$sdp.tagsOfAsset('asset', 'pagitatorParam').then(function(result){
//	 * 			// Result is a PaginatorPage<SdpTag>
//	 * 			// Do something with result.
//	 * 		});
//	 * </code></pre>
//	 */
//	this.tagsOfAsset = function(asset, pp){
//		return seen.createFind({
//			method : 'GET',
//			url : '/api/sdp/asset/'+asset.id+'/tag/find',
//		}, _tagCache)(pp);
//	};
//	
//	/**
//	 * دارایی‌هایی را که ب یک دارایی در ارتباط هستند (و در انتهای آن ارتباط قرار دارند) برمی‌گرداند.
//	 * فراخوانی این تابع به صورت زیر است:
//	 * <pre><code>
//	 * 		$sdp.relatedAssetsToAsset('asset', 'pagitatorParam').then(function(result){
//	 * 			// Result is a PaginatorPage<SdpAsset>
//	 * 			// Do something with result.
//	 * 		});
//	 * </code></pre>
//	 */
//	this.relatedAssetsToAsset = function(asset, pp){
//		return seen.createFind({
//			method : 'GET',
//			url : '/api/sdp/asset/'+asset.id+'/relation/find',
//		}, _assetCache)(pp);
//	};
//	
//	/**
//	 * دارایی‌هایی که در یک دسته قرار دارد را برمی‌گرداند.
//	 * فراخوانی این تابع به صورت زیر است:
//	 * <pre><code>
//	 * 		$sdp.assetsOfCategory('category', 'pagitatorParam').then(function(result){
//	 * 			// Result is a PaginatorPage<SdpAsset>
//	 * 			// Do something with result.
//	 * 		});
//	 * </code></pre>
//	 */
//	this.assetsOfCategory = function(category, pp){
//		return seen.createFind({
//			method : 'GET',
//			url : '/api/sdp/category/'+category.id+'/asset/find',
//		}, _assetCache)(pp);
//	};
//	
//	/**
//	 * دارایی‌هایی که برچسب داده شده روی آن‌ها زده شده است را برمی‌گرداند.
//	 * فراخوانی این تابع به صورت زیر است:
//	 * <pre><code>
//	 * 		$sdp.assetsOfTag('tag', 'pagitatorParam').then(function(result){
//	 * 			// Result is a PaginatorPage<SdpAsset>
//	 * 			// Do something with result.
//	 * 		});
//	 * </code></pre>
//	 */
//	this.assetsOfTag = function(tag, pp){
//		return seen.createFind({
//			method : 'GET',
//			url : '/api/sdp/tag/'+tag.id+'/asset/find',
//		}, _assetCache)(pp);
//	};
//	
//});
