///*
// * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('seen-sdp')
///**
// * @ngdoc function
// * @name sdpLink
// * @description # sdpLink Controller of the angularSaasdmApp
// */
//.factory('SdpLink', function(PObject, PReceipt, PObjectFactory, $injector, seen) {
//
//	var _receiptFactory = new PObjectFactory(function(data) {
//		return new PReceipt(data);
//    });
//	
//	var _linkFactory = new PObjectFactory(function(data) {
//	    if(!this.sdpAssetInjector){
//	    	this.sdpLinkInjector = $injector.get('SdpLink');
//	    }
//	    return new this.sdpLinkInjector(data);
//	});
//	
//	var sdpLink = function() {
//		PObject.apply(this, arguments);
//	};
//	sdpLink.prototype = new PObject();
//
//	// adds new methods
//	sdpLink.prototype.update = seen.createUpdate({
//		method : 'POST',
//		url : '/api/sdp/link/:id',
//	});
//
//	sdpLink.prototype.delete = seen.createDelete({
//		method : 'DELETE',
//		url : '/api/sdp/link/:id'
//	});
//	
//	sdpLink.prototype.pay = seen.createNew({
//		method : 'POST',
//		url : '/api/sdp/link/:id/pay'
//	}, _receiptFactory);
//		
//	sdpLink.prototype.activate = seen.createGet({
//		method : 'GET',
//		url : '/api/sdp/link/:id/activate'
//	}, _linkFactory);
//	
//// function(backend, callbackUrl){
//// if(!backend.id){
//// // TODO: Hadi, show invalid gate error.
//// }
//// var req = {
//// url : '/api/sdp/link/' + this.id + '/pay',
//// method : 'POST'
//// };
//// req.headers = {
//// 'Content-Type' : 'application/x-www-form-urlencoded'
//// };
//// var data = {
//// backend : backend.id,
//// callback : callbackUrl,
//// 'XDEBUG_SESSION_START' : 'ECLIPSE_DBGP'
//// };
//// req.data = $httpParamSerializerJQLike(data);
//// return $http(req)//
//// .then(function(res) {
//// return res.data;
//// });
//// };
//	
//	sdpLink.prototype.isActive = function(){
//		return this.active === true;
//	};
//	
//	return sdpLink;
//});
