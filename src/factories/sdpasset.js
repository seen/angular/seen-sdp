///*
// * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//angular.module('seen-sdp')
//
///**
// * @ngdoc factory
// * @name sdpAsset
// * @description # sdpAsset Controller of the angularSaasdmApp
// */
//
//.factory('SdpAsset', function(PObject, PObjectFactory, $injector, $q, seen, $httpParamSerializerJQLike, $http) {
//    
//    	/**
//	 * سازنده برای تولید SdpAsset ها
//	 */
//        var _assetFactory = new PObjectFactory(function(data) {
//            if(!this.sdpAssetInjector){
//        	this.sdpAssetInjector = $injector.get('SdpAsset');
//            }
//            return new this.sdpAssetInjector(data);
//        });
//    
//	var sdpAsset = function() {
//		PObject.apply(this, arguments);
//	};
//	
//	sdpAsset.prototype = new PObject();
//
//	sdpAsset.prototype.update = seen.createUpdate({
//		method : 'POST',
//		url : '/api/sdp/asset/:id',
//	});
//	
//	sdpAsset.prototype.updateFileOfAsset = function(file) {
//		var formData = new FormData();
//		var objectData = this;
//		for(var key in objectData){
//			if(key === 'id'){
//				continue;
//			}
//			if(objectData[key]){
//				formData.append(key, objectData[key]);
//			}
//		}
//		formData.append('file', file);
//		return $http.post('/api/sdp/asset/' + this.id, formData, {
//			transformRequest : angular.identity,
//			headers : {
//				'Content-Type' : undefined
//			}
//		})//
//		.then(function(res) {
//			PObject.apply(this, res);
//			return res;
//		});
//	};
//
//	sdpAsset.prototype.delete = seen.createDelete({
//		method : 'DELETE',
//		url : '/api/sdp/asset/:id'
//	});
//	
//	sdpAsset.prototype.deleteRelationWith = function(asset){
//		return seen.createDelete({
//			method: 'DELETE',
//			url: '/api/sdp/asset/' + this.id + '/relation/' + asset.id
//		})();
//	};
//	
//	sdpAsset.prototype.getParent = seen.createGet({
//		method: 'GET',
//		url : '/api/sdp/asset/:parent'
//	}, _assetFactory);
//	
//	sdpAsset.prototype.hasParent = function(){
//	    return this.parent;
//	};
//	return sdpAsset;
//});