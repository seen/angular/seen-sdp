///*
// * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//angular.module('seen-sdp')
//
///**
// * @ngdoc factory
// * @name sdpAssetRelation
// * @description # sdpAssetRelation Controller of the angularSaasdmApp
// */
//
//.factory('SdpAssetRelation', function(PObject, PObjectFactory, PObjectCache, SdpAsset, $injector, $q, seen) {
//    
//	var _assetCache = new PObjectCache(function(data) {
//		return new SdpAsset(data);
//	});
//
//	var sdpAssetRelation = function() {
//		PObject.apply(this, arguments);
//	};
//	
//	sdpAssetRelation.prototype = new PObject();
//
//	sdpAssetRelation.prototype.update = seen.createUpdate({
//		method : 'POST',
//		url : '/api/sdp/assetrelation/:id',
//	});
//
//	sdpAssetRelation.prototype.delete = seen.createDelete({
//		method : 'DELETE',
//		url : '/api/sdp/assetrelation/:id'
//	});
//	
//	sdpAssetRelation.prototype.getStart = seen.createGet({
//		method: 'GET',
//		url : '/api/sdp/asset/:start'
//	}, _assetCache);
//	
//	sdpAssetRelation.prototype.getEnd = seen.createGet({
//		method: 'GET',
//		url : '/api/sdp/asset/:end'
//	}, _assetCache);
//	
//	return sdpAssetRelation;
//});